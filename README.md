# [unbundle](https://www.npmjs.com/package/unbundle) with [yarn](https://www.npmjs.com/package/yarn) in [C9.io](https://c9.io) workspace

## Initial Setup

- node v6.9.x
- install yarn 0.18.0
- initialize project

```sh
# setting node environment
nvm install 6
nvm alias default 6
node --version # v6.9.x

# installing yarn
curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --version 0.18.0

# Open a new terminal
yarn --version # 0.18.0

# initialize project
yarn init -y # -y means accepting all the default configurations
mkdir src
# $ tree src # expecting app.js and index.html
# src
# ├── app.js
# └── index.html

# install dependencies
yarn add react react-dom
yarn add unbundle --dev --ignore-scripts
yarn install

# create public directory through npm scripts
yarn run build

```